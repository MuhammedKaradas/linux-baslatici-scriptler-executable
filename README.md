***Script Çalıştırma***

> -> Öncelikle scripte executable haline gelmesi için "chmod +x <script_ismi>" komutunu vermeliyiz. Daha sonra bulunduğumuz dizinden çalıştırabiliriz. 
<hr>

***Scripti Dizin Bağımsız Çalıştırma***

> -> Scripti bulunduğu dizin dışındada çalıştırmak istiyorsak /usr/bin dizinine kopyalamalıyız.